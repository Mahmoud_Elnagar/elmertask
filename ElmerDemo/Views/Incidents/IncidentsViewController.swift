//
//  IncidentsViewController.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class IncidentsViewController: ParentViewController {

    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var viewFilters: UIView!
    @IBOutlet weak var viewDatePicker: UIView!
    @IBOutlet weak var btnSelectedDate: UIButton!
    @IBOutlet weak var btnSelectedStatus: UIButton!
    @IBOutlet weak var datePicker: UIDatePicker!

    var currentDate: Date? = nil
    var currentStatus: Int? = nil
    
    var viewModel: IncidentsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Incidents"
        self.viewModel = IncidentsViewModel(delgateVC: self)
        
        self.viewDatePicker.isHidden = true
        self.viewFilters.isHidden = true
        self.updateData()
    }
    
    @IBAction func chooseNewIncidentAction(_ sender: UIBarButtonItem)
    {

    }
    
    @IBAction func goToSettingsAction(_ sender: UIBarButtonItem)
    {
        self.viewModel.goToSettingsView()
    }
    
    // MARK: filter actions

    @IBAction func openFiltersAction(_ sender: UIBarButtonItem)
    {
        self.viewDatePicker.isHidden = true
        self.viewFilters.isHidden = false
    }
    
    @IBAction func closeFiltersAction(_ sender: UITapGestureRecognizer) {
        self.viewDatePicker.isHidden = true
        self.viewFilters.isHidden = true
    }
    
    @IBAction func resetFiltersAction(_ sender: UIBarButtonItem)
    {
        currentStatus = nil
        currentDate = nil
        self.updateData()
    }
    
    @IBAction func selectFilterDateAction(_ sender: UIBarButtonItem)
    {
        self.viewDatePicker.isHidden = false
    }
    
    @IBAction func selectFilterStatusAction(_ sender: UIBarButtonItem)
    {
        self.currentDate = nil
        let actionSheet = UIAlertController(title: "Please select status", message: nil, preferredStyle: .actionSheet)
        
        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.currentStatus = nil
            self.updateData()
        }
        actionSheet.addAction(cancelActionButton)
        
        let submittedActionButton = UIAlertAction(title: "Submitted", style: .default)
        { _ in
            self.currentStatus = 0
            self.updateData()
        }
        actionSheet.addAction(submittedActionButton)
        
        let inProgressActionButton = UIAlertAction(title: "InProgress", style: .default)
        { _ in
            self.currentStatus = 1
            self.updateData()
        }
        
        actionSheet.addAction(inProgressActionButton)
        
        let completedActionButton = UIAlertAction(title: "Completed", style: .default)
        { _ in
            self.currentStatus = 2
            self.updateData()
        }
        actionSheet.addAction(completedActionButton)
        
        let rejectedActionButton = UIAlertAction(title: "Rejected", style: .default)
        { _ in
            self.currentStatus = 3
            self.updateData()
        }
        actionSheet.addAction(rejectedActionButton)
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    @IBAction func datePickerDoneAction(_ sender: UIDatePicker) {
        self.currentStatus = nil
        self.currentDate = datePicker.date
        self.updateData()
    }
    
    func updateData()
    {
        self.viewModel.getIncidents(date: self.currentDate, status: self.currentStatus)
        self.viewDatePicker.isHidden = true
        self.viewFilters.isHidden = true
    }
}
    
extension IncidentsViewController: ViewModelParentDelegate
{
    func requestFailedAndShowMessage(message: String)
    {
        self.showAlert(message: message)
    }
    
    func requestSuccess()
    {
        self.tableView.reloadData()
    }
    
    func showLoader()
    {
        self.showElmLoader()
    }
    
    func hideLoader()
    {
        self.hideElmLoader()
    }
    
    func showViewController(viewController: UIViewController)
    {
        self.show(viewController, sender: self)
    }
}

// MARK: - UITableViewDelegate

extension IncidentsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.viewModel.filteredIncidentsViewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IncidentTableViewCell", for: indexPath) as! IncidentTableViewCell
        
        cell.configMessage(model: self.viewModel.filteredIncidentsViewModels[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {

    }
}
