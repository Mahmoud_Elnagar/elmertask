//
//  IncidentTableViewCell.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class IncidentTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDescription: TitleLabelWithColor!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var imgView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configMessage(model: IncidentViewModel)
    {
        self.lblDescription.text = model.incidentDescription
        self.lblCreatedAt.text = "Created at: " + model.createdAt
        self.lblStatus.text = "Status: " + model.statusDescription
    }
}
