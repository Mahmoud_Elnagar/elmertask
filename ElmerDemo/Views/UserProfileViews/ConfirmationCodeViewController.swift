//
//  ConfirmationCodeViewController.swift
//  CryptoOffice
//
//  Created by Mahmoud Elnagar on 08/11/2021.
//

import UIKit

class ConfirmationCodeViewController: ParentViewController
{
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtConfirm: UITextField!
    @IBOutlet weak var lblMessage: UILabel!
    
    var initialMessage = ""
    var email = ""
    var viewModel: UserAuthorizationViewModel!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationItem.title = "Confirm"
        self.viewModel = UserAuthorizationViewModel(delgateVC: self)
//        self.lblMessage.text = initialMessage
        self.txtEmail.text = email
        self.txtConfirm.text = "4242"
    }
    
    @IBAction func userClickedConfirmAction(_ sender: UIButton)
    {
        viewModel.confirmUserEmailWith(email: email, otp: txtConfirm.text)
    }
}

extension ConfirmationCodeViewController: ViewModelParentDelegate
{
    func requestFailedAndShowMessage(message: String)
    {
        self.lblMessage.text = message
    }
    
    func requestSuccess()
    {}
    
    func showLoader()
    {
        self.showElmLoader()
    }
    
    func hideLoader()
    {
        self.hideElmLoader()
    }
    
    func setRootViewController(viewController: UIViewController) {
        if let window = self.view.window
        {
            window.rootViewController = viewController
            window.makeKeyAndVisible()
        }
    }
}
