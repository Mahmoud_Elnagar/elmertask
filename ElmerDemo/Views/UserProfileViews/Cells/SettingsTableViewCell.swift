//
//  SettingsTableViewCell.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class SettingsTableViewCell: UITableViewCell
{
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgIcon: UIImageView!

    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configCell(viewModel: SettingsCard)
    {
        self.lblTitle.text = viewModel.displayTitle
        self.imgIcon.image = viewModel.icon
    }
}
