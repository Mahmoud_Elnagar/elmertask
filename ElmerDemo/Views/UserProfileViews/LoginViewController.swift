//
//  LoginViewController.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class LoginViewController: ParentViewController
{
    @IBOutlet weak var txtEmail: UITextField!
    var viewModel: UserAuthorizationViewModel!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationItem.title = "Login"
        self.txtEmail.text = "mahmoudel_nagar@icloud.com"
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewModel = UserAuthorizationViewModel(delgateVC: self)
    }
    
    @IBAction func userClickedLoginAction(_ sender: UIButton)
    {
        viewModel.logUserInWith(email: self.txtEmail.text)
    }
}

extension LoginViewController: ViewModelParentDelegate
{
    func requestFailedAndShowMessage(message: String)
    {
        self.showAlert(message: message)
    }
    
    func requestSuccess()
    {
        
    }
    
    func showLoader()
    {
        self.showElmLoader()
    }
    
    func hideLoader()
    {
        self.hideElmLoader()
    }
    
    
    func showViewController(viewController: UIViewController) {
        self.show(viewController, sender: self)
    }
}
