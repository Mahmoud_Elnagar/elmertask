//
//  SettingsViewController.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class SettingsViewController: ParentViewController
{
    @IBOutlet weak var tableView: UITableView!
    var viewModel: AccountSettingsViewModel!

    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.navigationItem.title = "Settings"
        self.viewModel = AccountSettingsViewModel(delgateVC: self)
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.viewModel.settingsItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsTableViewCell", for: indexPath) as! SettingsTableViewCell
        
        cell.configCell(viewModel: self.viewModel.settingsItems[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        self.viewModel.goToViewAtIndex(index: indexPath.row)
    }
}

extension SettingsViewController: ViewModelParentDelegate
{
    func requestFailedAndShowMessage(message: String)
    {
        self.showAlert(message: message)
    }
    
    func requestSuccess()
    {
        
    }
    
    func showLoader()
    {
        self.showElmLoader()
    }
    
    func hideLoader()
    {
        self.hideElmLoader()
    }
    
    func showViewController(viewController: UIViewController)
    {
        if viewController is UINavigationController
        {
            if let window = self.view.window
            {
                User.shared().logoutUser()
                window.rootViewController = viewController
                window.makeKeyAndVisible()
            }
        }
        else
        {
            self.show(viewController, sender: self)
        }
    }
}
