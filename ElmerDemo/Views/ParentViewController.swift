//
//  ParentViewController.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit
import MBProgressHUD
import Reachability

class ParentViewController: UIViewController
{
    @IBOutlet weak var btn_back: UIButton!
    var shouldShowEmptyStateText = true
    var stateText = ""
    var isNeedLocalization:Bool = true
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

    }
    
    func showAlert(message: String?, shouldpop: Bool = false)
    {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alertController.view.tintColor = .mainColor
        
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action) in
            alertController.dismiss(animated: true, completion: nil)
            
            if shouldpop
            {
                if let navController = self.navigationController
                {
                    navController.popViewController(animated: true)
                }
                else
                {
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
        alertController.addAction(OKAction)
        
        self.present(alertController, animated: true)
    }
    
    func showElmLoader()
    {
        DispatchQueue.main.async {
            let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
            
            hud.bezelView.color = .mainColor
            hud.bezelView.style = .solidColor
            hud.activityIndicatorColor = .white
        }
    }
    
    func hideElmLoader()
    {
        DispatchQueue.main.async {
            MBProgressHUD.hide(for: self.view, animated: true)
        }
        
    }
}

