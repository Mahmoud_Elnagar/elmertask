//
//  CorneredView.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class CorneredView: UIView
{
    var cornerRadius = 8.0
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.layer.cornerRadius = cornerRadius
        self.clipsToBounds = true
    }
}
