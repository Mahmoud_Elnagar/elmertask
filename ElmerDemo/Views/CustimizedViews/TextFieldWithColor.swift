//
//  TextFieldWithColor.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class TextFieldWithColor: UITextField
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.textColor = .darkGray
    }
}
