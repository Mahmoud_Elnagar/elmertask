//
//  TitleLabelWithColor.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class TitleLabelWithColor: UILabel
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.textColor = .mainColor
        self.font = UIFont.systemFont(ofSize: 17.0, weight: .semibold)
    }

}
