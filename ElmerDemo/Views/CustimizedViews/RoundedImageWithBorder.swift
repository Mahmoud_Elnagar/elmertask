//
//  RoundedImageWithBorder.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class RoundedImageWithBorder: UIImageView
{
    var shouldBorder = false
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.layer.cornerRadius = (self.frame.width / 2)
        self.clipsToBounds = true
        
        if shouldBorder
        {
            self.layer.borderColor = UIColor.darkGray.cgColor
            self.layer.borderWidth = 0.5
        }        
    }
}
