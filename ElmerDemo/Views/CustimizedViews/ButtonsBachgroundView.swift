//
//  ButtonsBachgroundView.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class ButtonsBachgroundView: UIView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        self.backgroundColor = UIColor.mainColor
        self.layer.cornerRadius = 5
        self.clipsToBounds = true
    }
}
