//
//  NormalButton.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class NormalButton: UIButton
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        if let image = self.image(for: .normal)
        {
            let imageWithRenderingMode = image.withRenderingMode(.alwaysTemplate)
            self.setImage(imageWithRenderingMode, for: .normal)
            
            self.tintColor = UIColor.mainColor
        }
    }
}
