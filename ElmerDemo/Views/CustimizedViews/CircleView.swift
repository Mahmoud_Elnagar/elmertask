//
//  CircleView.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class CircleView: UIView
{
    override func layoutSubviews()
    {
        super.layoutSubviews()
        
        self.layer.cornerRadius = (self.frame.width / 2)
        self.clipsToBounds = true
        self.backgroundColor = .mainColor
    }
}
