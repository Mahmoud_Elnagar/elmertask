//
//  ViewControllerMapper.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import Foundation
import UIKit

enum ControllerNames: String
{
    case loginNavVC = "LoginViewControllerNav"
    case confirmationCodeVC = "ConfirmationCodeViewController"
    case incidentsNavVC = "IncidentsHomeViewControllerNav"
    case settingsVC = "SettingsViewController"
}

class ViewControllerMapper: NSObject
{
    private static var currentService: ViewControllerMapper?
    var allViewControllerssConfigurations: Dictionary<String, Any>?
    
    class func shared() -> ViewControllerMapper
    {
        if let current = self.currentService
        {
            return current
        }
        else
        {
            return ViewControllerMapper()
        }
    }
    
    private override init()
    {
        super.init()
        
        let path = "ViewController-Mapper"
        if let configurationsPath = Bundle.main.path(forResource: path, ofType: "plist")
        {
            if let configurations = NSMutableDictionary(contentsOfFile: configurationsPath) as? Dictionary<String, Any>
            {
                self.allViewControllerssConfigurations = configurations
            }
        }
    }
    
    private func constructViewConfiguration(configuration: [String : String]) -> UIViewController?
    {
        if let storyboardName = configuration["Storyboard"], let viewControllerName = configuration["ViewIdentifier"], storyboardName.count > 0, viewControllerName.count > 0
        {
            let storyboard = UIStoryboard(name: storyboardName, bundle: .main)
            let viewController = storyboard.instantiateViewController(withIdentifier: viewControllerName)
            
            return viewController
        }
        
        return nil
    }
    
    func getViewController(identifier: ControllerNames) -> UIViewController?
    {
        if let _allViewControllerssConfigurations = self.allViewControllerssConfigurations, let vcConfiguration = _allViewControllerssConfigurations[identifier.rawValue] as? [String : String]
        {
            let viewController = self.constructViewConfiguration(configuration: vcConfiguration)
            return viewController
        }
        
        return nil
    }
}
