//
//  Extentions.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import Foundation
import UIKit

extension UIColor
{
    class var mainColor: UIColor
    {
        //#37467F
        return UIColor(red: 55.0/255, green: 70.0/255, blue: 127.0/255, alpha: 1)
    }
    
    class var supportColor: UIColor
    {
        //#37B29B
        return UIColor(red: 55.0/255, green: 178.0/255, blue: 155.0/255, alpha: 1)
    }
}
