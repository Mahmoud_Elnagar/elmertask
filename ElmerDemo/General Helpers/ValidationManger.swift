//
//  ValidationManger.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

struct ValidationManger
{
    private let emailPattern = #"^\S+@\S+\.\S+$"#
    
    func validateEmailAddress(email: String?) -> Bool
    {
        return generalValidation(pattern: emailPattern, text: email)
    }
    
    func generalValidation(pattern: String?, text: String?) -> Bool
    {
        if let patternString = pattern, let testedString = text, patternString.count > 0, testedString.count > 0
        {
            let patternRegex = try! NSRegularExpression(
                pattern: patternString,
                options: []
            )
            
            let sourceRange = NSRange(
                testedString.startIndex..<testedString.endIndex,
                in: testedString
            )
            
            let result = patternRegex.matches(
                in: testedString,
                options: [],
                range: sourceRange
            )

            return !result.isEmpty
        }
        
        return false
    }
}
