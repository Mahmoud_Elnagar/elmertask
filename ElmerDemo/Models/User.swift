//
//  User.swift
//
//  Created by Mahmoud Elnagar on 4/12/18.
//

import Foundation
import SwiftyJSON

class User: NSObject, NSCoding
{
    var token, email: String?
    
    private static var currentUser: User!
    
    private override init() { }
    
    class func shared() -> User
    {
        if self.currentUser == nil
        {
            if let cachedUserData = UserDefaults.standard.object(forKey: "UserCachedData") as? Data
            {
                do
                {
                    let cachedUser = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(cachedUserData) as! User
                    
                    return cachedUser
                }
                catch
                {
                    return User()
                }
            }
            else
            {
                return User()
            }
        }
        else
        {
            return self.currentUser
        }
    }
    
    func encode(with aCoder: NSCoder)
    {
        aCoder.encode(self.token, forKey: "token")
        aCoder.encode(self.email, forKey: "email")
        
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        self.token = aDecoder.decodeObject(forKey: "token") as? String
        self.email = aDecoder.decodeObject(forKey: "email") as? String
        
    }
    
    init(parametersJson: [String : JSON])
    {
        email = ""
        if let token = parametersJson["token"]?.string {
            self.token = "Bearer  \(token)"
        }
    }
    
    func saveUser()
    {
        do
        {
            let encodedObject = try NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: false)
            UserDefaults.standard.set(encodedObject, forKey: "UserCachedData")
            UserDefaults.standard.synchronize()
        }
        catch
        {
            print("Save User failed")
        }
    }
    
    func logoutUser()
    {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "UserCachedData")
    }
    
    func isLoggedIn() -> Bool
    {
        if let authToken = self.token, authToken.count > 0
        {
            return true
        }
        
        return false
    }
}
