//
//  SettingsCard.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class SettingsCard: NSObject
{
    var displayTitle: String
    var icon: UIImage?
    var navigateTo: ControllerNames
    
    //Dependancy Injection
    init(displayTitle: String, icon: UIImage?, navigateTo: ControllerNames)
    {
        self.displayTitle = displayTitle
        self.icon = icon
        self.navigateTo = navigateTo
    }
}
