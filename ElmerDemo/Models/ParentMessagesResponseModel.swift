//
//  GeneralResponseModel.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import Foundation

// MARK: - Parents

struct IncidentsResponseModel: Decodable
{
    let baseURL: String?
    let incidents: [IncidentModel]?
}

struct IncidentModel: Decodable
{
    let incidentId, incidentDescription, issuerId, createdAt, updatedAt, assigneeId: String?
    let latitude, longitude: Double?
    let status, priority, typeId: Int?
    let medias: [MediaModel]?
    
    enum CodingKeys: String, CodingKey
    {
        case incidentId = "id"
        case incidentDescription = "description"
        case issuerId = "issuerId"
        case createdAt = "createdAt"
        case updatedAt = "updatedAt"
        case assigneeId = "assigneeId"
        
        case latitude = "latitude"
        case longitude = "longitude"
        
        case status = "status"
        case priority = "priority"
        case typeId = "typeId"
        case medias = "medias"
    }
}

struct MediaModel: Decodable
{
    let mediaId, mimeType, url, incidentId: String?
    let mediaType: Int?
    
    enum CodingKeys: String, CodingKey
    {
        case mediaId = "id"
        case mimeType = "mimeType"
        case url = "url"
        case incidentId = "incidentId"
        case mediaType = "type"
    }
}
