//
//  AccountSettingsViewModel.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class AccountSettingsViewModel: NSObject
{
    var delegate: ViewModelParentDelegate
    var settingsItems = [SettingsCard]()

    init(delgateVC: ViewModelParentDelegate)
    {
        self.delegate = delgateVC
        
        self.settingsItems.append(SettingsCard(displayTitle: "Logout", icon: UIImage(named: "logout"), navigateTo: .loginNavVC))
    }
    
    func goToViewAtIndex(index: Int)
    {
        let item = settingsItems[index]
        if let viewController =  ViewControllerMapper.shared().getViewController(identifier: item.navigateTo)
        {
            if self.delegate.showViewController != nil
            {
                self.delegate.showViewController!(viewController: viewController)
            }
        }
    }
}
