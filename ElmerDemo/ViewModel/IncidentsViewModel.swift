//
//  IncidentsViewModel.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class IncidentsViewModel: NSObject
{
    var delegate: ViewModelParentDelegate
    var incidentsViewModels = [IncidentViewModel]()
    var filteredIncidentsViewModels = [IncidentViewModel]()

    init(delgateVC: ViewModelParentDelegate)
    {
        self.delegate = delgateVC
    }
    
    func goToSettingsView()
    {
        if let viewController =  ViewControllerMapper.shared().getViewController(identifier: .settingsVC)
        {
            if self.delegate.showViewController != nil
            {
                self.delegate.showViewController!(viewController: viewController)
            }
        }
    }
    
    func getIncidents(date: Date?, status: Int?)
    {
        if self.incidentsViewModels.count > 0
        {
            // already data exists
            if date == nil && status == nil {
                self.filteredIncidentsViewModels = self.incidentsViewModels
            }
            else {
                if date != nil {
                    self.filteredIncidentsViewModels = self.incidentsViewModels.filter { $0.date >= date! }
                }
                
                if status != nil
                {
                    self.filteredIncidentsViewModels = self.incidentsViewModels.filter { $0.status >= status! }
                }
            }
            self.delegate.requestSuccess()
        }
        else
        {
            // get data for the first time
            self.getIncidentsOnline()
        }
    }
    
    func getIncidentsOnline()
    {
        self.delegate.showLoader()
        IncidentsLogicLayer().getIncidents { [weak self] incidents in
            
            let incidentsVMs = incidents.map({ return IncidentViewModel(model: $0) })
            self?.incidentsViewModels = incidentsVMs.sorted { $0.date > $1.date }
            self?.filteredIncidentsViewModels = self?.incidentsViewModels ?? []
            
            self?.delegate.hideLoader()
            self?.delegate.requestSuccess()
        } onFailure: { [weak self] message in
            
            self?.delegate.hideLoader()
            self?.delegate.requestFailedAndShowMessage(message: message ?? "")
        }
    }
}
