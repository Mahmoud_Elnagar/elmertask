//
//  UserAuthorizationViewModel.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

@objc protocol ViewModelParentDelegate
{
    func showLoader()
    func hideLoader()
    func requestSuccess()
    func requestFailedAndShowMessage(message: String)
    @objc optional func showViewController(viewController: UIViewController)
    @objc optional func setRootViewController(viewController: UIViewController)
}

class UserAuthorizationViewModel: NSObject
{
    private let validationManager = ValidationManger()
    var delegate: ViewModelParentDelegate
    var logicLayer = UserProfileLogicLayer()
    
    init(delgateVC: ViewModelParentDelegate)
    {
        self.delegate = delgateVC
    }
    
    func logUserInWith(email: String?)
    {
        if !validationManager.validateEmailAddress(email: email) {
            self.delegate.requestFailedAndShowMessage(message: "kindly, enter valid Email.")
            return
        }
        
        self.delegate.showLoader()
        logicLayer.loginUserWith(email: email!) { [weak self] in
            
            self?.delegate.hideLoader()
            if let viewController =  ViewControllerMapper.shared().getViewController(identifier: .confirmationCodeVC)
            {
                if self?.delegate.showViewController != nil {
                    self?.delegate.showViewController!(viewController: viewController)
                }
            }
                
        } onFailure: { message in
            
            self.delegate.hideLoader()
            self.delegate.requestFailedAndShowMessage(message: message ?? "")
        }
    }
    
    func confirmUserEmailWith(email: String, otp: String?)
    {
        if otp == nil || otp!.isEmpty {
            self.delegate.requestFailedAndShowMessage(message: "kindly, fill in the code.")
            return
        }
        
        self.delegate.showLoader()
        logicLayer.verifyOTPWith(email: email, otp: otp ?? "") { [weak self] in
           
            if let viewController =  ViewControllerMapper.shared().getViewController(identifier: .incidentsNavVC)
            {
                if self?.delegate.setRootViewController != nil
                {
                    self?.delegate.setRootViewController!(viewController: viewController)
                }
            }
            
        } onFailure: { message in
            
            self.delegate.hideLoader()
            self.delegate.requestFailedAndShowMessage(message: message ?? "")
        }
    }
}
