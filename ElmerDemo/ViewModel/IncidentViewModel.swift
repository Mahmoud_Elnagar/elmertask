//
//  IncidentViewModel.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class IncidentViewModel: NSObject
{
    var incidentId = ""
    var incidentDescription = ""
    var createdAt = ""
    var statusDescription = ""
    var date = Date()
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var status: Int = 0
    var typeId: Int = 0
    var medias = [URL]()
    
    //Dependancy Injection
    init(model: IncidentModel)
    {
        self.incidentId = model.incidentId ?? ""
        self.incidentDescription = model.incidentDescription ?? ""
        self.createdAt = model.createdAt ?? ""
        self.latitude = model.latitude ?? 0.0
        self.longitude = model.longitude ?? 0.0
        self.status = model.status ?? 0
        self.typeId = model.typeId ?? 0
        
        switch self.status {
        case 1:
            self.statusDescription = "InProgress"
        case 2:
            self.statusDescription = "Completed"
        case 3:
            self.statusDescription = "Rejected"
        default:
            self.statusDescription = "Submitted"
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"

        if let date = formatter.date(from: self.createdAt)
        {
            self.date = date
            formatter.dateFormat = "EEE, dd-MM-yyyy"
            self.createdAt = formatter.string(from: date)
        }
        
        
        if let mediasModels = model.medias, medias.count > 0
        {
            for item in mediasModels
            {
                if let urlString = item.url, let url = URL(string: urlString)
                {
                    medias.append(url)
                }
            }
        }
    }
}
