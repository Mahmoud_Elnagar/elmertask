//
//  IncidentsLogicLayer.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit

class IncidentsLogicLayer: NSObject
{
    let sharedServiceManager = ServiceConfigurationBuilder.shared()

    func getIncidents(onSuccess: @escaping ([IncidentModel]) -> Void,
                      onFailure: @escaping (String?) -> Void ) -> Void {
        
        if isNetworkReachable
        {
            let apiService = sharedServiceManager.getServiceObject(serviceName: "getIncidents", apiParams: nil, embedData: nil)
            
            ServiceManager.callAPI(configurationRequest: apiService) { (error, response) in
                
                if response != nil
                {
                    do {
                        if let resultData = try response?.rawData()
                        {
                            let responseModel = try JSONDecoder().decode(IncidentsResponseModel.self, from: resultData)
                            
                            if let incidents = responseModel.incidents {
                                onSuccess(incidents)
                            }
                            else
                            {
                                onFailure("Oops, Error occured")
                            }
                        }
                        else
                        {
                            onFailure("Oops, Error occured")
                        }
                    } catch {
                        print("JSONSerialization error:", error)
                        onFailure("Oops, Error occured")
                    }
                }
                else
                {
                    onFailure("Oops, Error occured")
                }
            }
            
        }
        else
        {
            onFailure("Oops, Network Failure")
        }
    }
}
