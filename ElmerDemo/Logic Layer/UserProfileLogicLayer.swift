//
//  UserProfileLogicLayer.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit
import Alamofire

class UserProfileLogicLayer: NSObject
{
    private let sharedServiceManager = ServiceConfigurationBuilder.shared()
    
    func loginUserWith(email: String,
                       onSuccess: @escaping () -> Void,
                       onFailure: @escaping (String?) -> Void ) -> Void {
        if isNetworkReachable
        {            
            let params = [
                "email" : email
            ]
            
            let apiService = sharedServiceManager.getServiceObject(serviceName: "loginUser", apiParams: params, embedData: nil)
            
            ServiceManager.callAPI(configurationRequest: apiService, isResponseJson: false) { (error, response) in
                
                if response == "OK"
                {
                    onSuccess()
                }
                else
                {
                    onFailure("Oops, Error occured")
                }
            }
        }
        else
        {
            onFailure("Oops, Network Failure")
        }
    }
    
    func verifyOTPWith(email: String,
                              otp: String,
                              onSuccess: @escaping () -> Void,
                              onFailure: @escaping (String?) -> Void ) -> Void
    {
        if isNetworkReachable
        {
            let params = [
                "email" : email,
                "otp" : otp
            ]
            
            let apiService = sharedServiceManager.getServiceObject(serviceName: "verifyOTP", apiParams: params, embedData: nil)
            
            ServiceManager.callAPI(configurationRequest: apiService) { (error, response) in
                
                if let responseData = response?.dictionary
                {
                    let user = User(parametersJson: responseData)
                    user.email = email
                    user.saveUser()
                    onSuccess()
                }
                else
                {
                    onFailure("Oops, Error occured")
                }
            }
        }
        else
        {
            onFailure("Oops, Network Failure")
        }
    }
}
