//
//  APIRequestConfiguration.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import Foundation

class APIRequestConfiguration: NSObject
{
    var service_Method: String
    var service_URL: String
    var isLogEnabled = true
    var importServerHeader = true
    var jsonEncoding = true
    var requireAuthorization = true
    var basicHeaders = [String : String]()
    var parameters: [String : Any]?

    //Dependancy Injection
    init(parameters: [String : Any])
    {
        self.service_Method = "GET"
        self.service_URL = ""
        
        if let service_Method = parameters["service_Method"] as? String
        {
            self.service_Method = service_Method
        }
        
        if let service_URL = parameters["Service_URL"] as? String
        {
            self.service_URL = service_URL
        }
        
        if let isLogEnabled = parameters["isLogEnabled"] as? Bool
        {
            self.isLogEnabled = isLogEnabled
        }
        
        if let importServerHeader = parameters["importServerHeader"] as? Bool
        {
            self.importServerHeader = importServerHeader
        }
        
        if let jsonEncoding = parameters["jsonEncoding"] as? Bool
        {
            self.jsonEncoding = jsonEncoding
        }
        
        if let requireAuthorization = parameters["requireAuthorization"] as? Bool
        {
            self.requireAuthorization = requireAuthorization
        }
    }
        
    func addMoreConfiguration(serverConfiguration: [String : Any]?, apiParameters: [String : Any]?, embedData: [String?]?)
    {
        if let serverConfig = serverConfiguration
        {
            var fullBaseURl = ""
            if let baseURl = serverConfig["BaseURL"] as? String
            {
                fullBaseURl = baseURl
            }
            
            let languageName = "Language-En"
            
            if let languageAdded = serverConfig[languageName] as? String, languageAdded.count > 0
            {
                fullBaseURl = fullBaseURl + languageAdded
            }
            
            fullBaseURl += self.service_URL
            
            if let apiParametersDictionary = apiParameters, apiParametersDictionary.keys.count > 0
            {
                if self.service_Method == "GET"
                {
                    fullBaseURl = "\(fullBaseURl)\(self.convertDictionaryToGETParameters(paramsDictionary: apiParametersDictionary))"
                }
                else
                {
                    self.parameters = apiParametersDictionary
                }
            }
            
            if self.importServerHeader == true
            {
                if let headersDic = serverConfig["Headers"] as? Dictionary<String, String>
                {
                    self.basicHeaders = headersDic
                }
            }
            
            if let insertedArray = embedData, insertedArray.count > 0, fullBaseURl.contains("%@")
            {
                for index in 0 ..< insertedArray.count
                {
                    if let range = fullBaseURl.range(of: "%@")
                    {
                        if let insertedString = insertedArray[index]
                        {
                            fullBaseURl = fullBaseURl.replacingCharacters(in: range, with: insertedString)
                        }
                        else
                        {
                            fullBaseURl.removeSubrange(range)
                        }
                    }
                    else
                    {
                        break
                    }
                }
            }
                        
            self.service_URL = fullBaseURl
        }
    }
    
    func convertDictionaryToGETParameters(paramsDictionary: [String : Any]) -> String
    {
        var fullParameters = ""
        for (key, value) in paramsDictionary
        {
            fullParameters = "\(fullParameters)\(key)=\(value)&"
        }
        
        fullParameters = String(fullParameters.prefix(fullParameters.count - 1))
        return "?\(fullParameters)"
    }
}
