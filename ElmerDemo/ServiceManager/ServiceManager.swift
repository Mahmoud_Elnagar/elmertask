//
//  ServiceManager.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import UIKit
import Alamofire
import SwiftyJSON

class ServiceManager: NSObject
{
    typealias ApiResponse = (Error?, JSON?) -> Void
    
    class func callAPI(configurationRequest: APIRequestConfiguration?, isResponseJson: Bool = true, onCompletion: @escaping ApiResponse) -> Void
    {
        if let configRequest = configurationRequest
        {
            var headers = configRequest.basicHeaders

            if configRequest.requireAuthorization && User.shared().isLoggedIn() {
                headers["Authorization"] = User.shared().token ?? ""
            }
            
            if configRequest.isLogEnabled
            {
                print("===============================================")
                print("url = \(configRequest.service_URL)")
                print("headers = \(headers)")
                print("parameters = \(configRequest.parameters ?? [:])")
                print("===============================================")
            }
            
            if let url = URL(string: configRequest.service_URL)
            {
                let request = Alamofire.request(
                    url,
                    method: HTTPMethod(rawValue: configRequest.service_Method) ?? .get,
                    parameters: configRequest.parameters,
                    encoding: (configRequest.jsonEncoding ? JSONEncoding.default : URLEncoding.default),
                    headers: headers)
                
                if isResponseJson
                {
                    self.callAPIWithJsonResponse(configurationRequest: request) { error, response in
                        
                        if configRequest.isLogEnabled
                        {
                            print(response ?? "No response")
                        }
                        
                        onCompletion(error, response)
                    }
                }
                else
                {
                    self.callAPIWithTextResponse(configurationRequest: request) { error, response in
                        
                        if configRequest.isLogEnabled
                        {
                            print(response ?? "No response")
                        }
                        
                        onCompletion(error, response)
                    }
                }
            }
            else
            {
                onCompletion(nil, nil)
            }
        }
        else
        {
            onCompletion(nil, nil)
        }
    }
    
    class func callAPIWithJsonResponse(configurationRequest: DataRequest,
                       onCompletion: @escaping ApiResponse) -> Void
    {
        configurationRequest.responseJSON { response in
            
            if let result = response.result.value
            {
                onCompletion(nil, JSON(result));
            }
            else
            {
                onCompletion(response.result.error, nil);
            }
        }
    }
    
    class func callAPIWithTextResponse(configurationRequest: DataRequest,
                       onCompletion: @escaping ApiResponse) -> Void
    {
        configurationRequest.responseString() { response in
            
            if let result = response.result.value
            {
                onCompletion(nil, JSON(result));
            }
            else
            {
                onCompletion(response.result.error, nil);
            }
        }
    }
    
    class func downloadFileFromURL(configurationRequest: APIRequestConfiguration?,
                                   fileSavingPath: String,
                                   onCompletion: @escaping (Error?, String?) -> Void) -> Void
    {
        if let configRequest = configurationRequest
        {
            var headers = configRequest.basicHeaders
            
            if configRequest.requireAuthorization &&
                User.shared().isLoggedIn()
            {
                headers["Authorization"] = User.shared().token ?? ""
            }
            
            if configRequest.isLogEnabled
            {
                print("===============================================")
                print("url = \(configRequest.service_URL)")
                print("headers = \(headers)")
                print("parameters = \(configRequest.parameters ?? [:])")
                print("===============================================")
            }
            
            if let url = URL(string: configRequest.service_URL)
            {
                let destination: DownloadRequest.DownloadFileDestination = { _, _ in
                    return (URL(fileURLWithPath: fileSavingPath), [.removePreviousFile])
                }
                
                Alamofire.download(
                    url,
                    method: HTTPMethod(rawValue: configRequest.service_Method) ?? .get,
                    parameters: configRequest.parameters,
                    encoding: (configRequest.jsonEncoding ? JSONEncoding.default : URLEncoding.default),
                    headers: headers,
                    to: destination).downloadProgress(closure: { (progress) in
                        //progress closure
                    }).response(completionHandler: { (downloadResponse) in
                        //here you able to access the DefaultDownloadResponse
                        //result closure
                        
                        onCompletion(nil, fileSavingPath)
                    })
            }
            else
            {
                onCompletion(nil, nil)
            }
        }
        else
        {
            onCompletion(nil, nil)
        }
    }
}
