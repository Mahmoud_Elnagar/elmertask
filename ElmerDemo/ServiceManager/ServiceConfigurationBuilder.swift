//
//  ServiceConfigurationBuilder.swift
//  ElmerDemo
//
//  Created by Mahmoud Elnagar on 20/11/2021.
//

import Foundation

class ServiceConfigurationBuilder: NSObject
{
    private static var currentService: ServiceConfigurationBuilder?
    
    var projectMode = "Production"
    var allConfigurations: Dictionary<String, Any>?
    var allServicesConfiguration: Dictionary<String, Dictionary<String, Any>>?
    
    class func shared() -> ServiceConfigurationBuilder
    {
        if let current = self.currentService
        {
            return current
        }
        else
        {
            return ServiceConfigurationBuilder()
        }
    }
    
    private override init()
    {
        super.init()
        
        let path = "API-Service-Configurations"
        if let configurationsPath = Bundle.main.path(forResource: path, ofType: "plist")
        {
            if let configurations = NSMutableDictionary(contentsOfFile: configurationsPath) as? Dictionary<String, Any>
            {
                self.allConfigurations = configurations
                if let mode = configurations["ProjectMode"] as? String
                {
                    self.projectMode = mode
                }
                
                let _servicesConfiguration = configurations["Services"] as? Dictionary<String, Dictionary<String, Any>>
                self.allServicesConfiguration = _servicesConfiguration
            }
        }
    }
    
    func getServiceObject(serviceName: String, apiParams: [String : Any]?, embedData: [String?]?) -> APIRequestConfiguration?
    {
        if let _allServicesConfiguration = self.allServicesConfiguration, let serviceConfiguration = _allServicesConfiguration[serviceName], let _allConfigurations = self.allConfigurations
        {
            let serviceObject = APIRequestConfiguration(parameters: serviceConfiguration)
            
            if let serverConfiguration = _allConfigurations[self.projectMode] as? [String : Any]
            {
                serviceObject.addMoreConfiguration(serverConfiguration: serverConfiguration, apiParameters: apiParams, embedData: embedData)
                
                return serviceObject
            }
        }
        
        return nil
    }
}
